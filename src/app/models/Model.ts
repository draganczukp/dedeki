export interface Combat {
	id: string;
	name: string;
	initiatives: Initiative[];
	currentRound: number;
}

export interface Initiative {
	id: string;
	combatant: PC | Enemy;
	roll: number;
	current?: boolean;
}

export interface PC {
	id: string;
	name: string;
	ac: number;
	hp: number;
	hp_max: number;
}

export interface Enemy extends PC {
	str: number;
	dex: number;
	con: number;
	int: number;
	wis: number;
	cha: number;
	speed: number;
	special_speed?: number;
	special_speed_type?: string;
	description?: string;
	attacks: Attack[];
}

export interface Attack {
	name: string;
	activate: "action" | "bonus" | "legendary_1" | "legendary_2" | "legendary_3" | "mythic";
	description: string;
	range: string;
	targets: string;
	bonus?: number;
	damage?: string;
	save?: number;
	saveType?: "str"|"dex"|"con"|"int"|"wis"|"cha";
}
