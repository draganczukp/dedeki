"use client";
import Header from "./components/Header";
import CharacterDetails from "./components/character-details/CharacterDetails";
import CharacterList from "./components/character-list/CharacterList";
import { StoreProvider } from "./state/StoreProvider";
import NextButton from "./components/NextButton";
import EditorList from "./components/edit/EditorList";
import EditorDetails from "./components/edit/EditorDetails";

export default function Home() {

  return (
    <StoreProvider>
      <div className="flex flex-col w-full h-screen bg-neutral-900 text-white">
        <Header />
        <main key="1" className="flex flex-col-reverse lg:flex-row lg:h-screen items-center lg:align-baseline lg:gap-8 lg:p-8 w-full bg-neutral-900 text-white">
          <EditorList />
          <EditorDetails />
          <CharacterList />
          <CharacterDetails />
          <NextButton />
        </main>
      </div>
    </StoreProvider>
  )
}

