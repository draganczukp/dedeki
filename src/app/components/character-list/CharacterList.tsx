import { Initiative } from "@/app/models/Model";
import { useAppDispatch, useAppSelector } from "@/app/state/hooks";
import { selectCurrentCombat } from "@/app/state/stateSlice";
import { useState } from "react";
import { damage as damageAction } from "@/app/state/stateSlice";
import classNames from "classnames";

function CharacterListEntry({ initiative }: { initiative: Initiative }) {
  const dispatch = useAppDispatch()
  const [damage, setDamage] = useState(0)
  const edit = useAppSelector((state) => state.globalState.edit);
  if (edit) return '';
  function doDamage() {
    dispatch(damageAction({ id: initiative.id, damage: damage }))
    setDamage(0);
  }
  return <div className={classNames(`flex items-center flex-col lg:flex-row justify-between hover:bg-neutral-700 px-4 py-3 rounded-md`, {
    'bg-emerald-600/50': initiative.current && initiative.combatant.hp > 0,
    'bg-neutral-800': !initiative.current && initiative.combatant.hp > 0,
    'bg-red-600/25': !initiative.current && initiative.combatant.hp <= 0,
    'bg-fuchsia-300': initiative.current && initiative.combatant.hp <= 0
  })}>
    <div className={classNames("flex items-center space-x-4", {
      "text-neutral-800": initiative.current && initiative.combatant.hp <= 0,
      "text-slate-100": !initiative.current && initiative.combatant.hp <= 0
    })}>
      <div className="flex items-center space-x-2">
        <span className="text-sm">Initiative</span>
        <span className="text-lg font-bold">{initiative.roll}</span>
      </div>
      <div className="flex items-center space-x-2">
        <span className="text-sm">AC</span>
        <span className="text-lg font-bold">{initiative.combatant.ac}</span>
      </div>
      <div className="flex items-center space-x-2">
        <span className="text-sm">HP</span>
        <span className="text-lg font-bold">{initiative.combatant.hp}/{initiative.combatant.hp_max}</span>
      </div>
      <span className="text-lg font-bold">{initiative.combatant.name}</span>
    </div>
    <div className="flex items-center space-x-4">
      <div className="flex items-center space-x-2">
        <input
          className="px-4 py-2 rounded-md w-24 bg-neutral-700 hover:bg-neutral-600 focus:outline-none focus:ring-2 focus:ring-neutral-500 text-white"
          placeholder="Damage"
          type="number"
          value={damage}
          onChange={(e) => setDamage(parseInt(e.target.value))}
        />
        <button className="px-4 py-2 rounded-md bg-red-200 text-stone-900 hover:bg-neutral-500 focus:outline-none focus:ring-2 focus:ring-neutral-400" onClick={() => doDamage()}>
          Damage
        </button>
      </div>
    </div>
  </div>
}

export default function CharacterList() {
  const currentCombat = useAppSelector(selectCurrentCombat);
  const edit = useAppSelector((state) => state.globalState.edit);
  if (edit) return '';

  return (
    <section className="w-full lg:w-3/4 px-6 pb-16 py-4 bg-neutral-800 rounded-l-lg">
      <h2 className="text-xl font-bold">{currentCombat?.name} - Round {currentCombat?.currentRound || 1}</h2>
      <div className="w-full max-w-5xl mt-4 space-y-4">
        {currentCombat?.initiatives.toSorted((a, b) => b.roll - a.roll).map((init, i) => {
          return <CharacterListEntry key={i} initiative={init} />
        })}
      </div>
    </section>
  );
}
