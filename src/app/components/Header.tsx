"use client"

import { useEffect, useRef } from "react";
import { useAppDispatch, useAppSelector } from "../state/hooks";
import { addCombat, endEdit, load, save, saveToDisk, selectCombats, selectCurrentCombat, setCurrentCombat, startEdit, upload } from "../state/stateSlice";
import { CopyPlus, FileDown, FileUp, Pencil, Save, SquarePlus } from "lucide-react";

export default function Header() {
  const combats = useAppSelector(selectCombats);
  const currentCombat = useAppSelector(selectCurrentCombat)
  const dispatch = useAppDispatch();
  const edit = useAppSelector((state) => state.globalState.edit);

  useEffect(() => {
    dispatch(load());
  }, []);

  const hiddenFile = useRef<HTMLInputElement>(null)

  const onUploadClick = (e: any) => {
    e.preventDefault()
    hiddenFile.current?.click();
  }

  const handleUpload = (file: File | null) => {
    if (!file) return;
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.result) {
        dispatch(upload(reader.result as string || '{}'));
      }
    }
    if (file) {
      reader.readAsText(file);
    }
  }

  return (
    <header className="w-full flex flex-col lg:flex-row items-center justify-between bg-neutral-700 py-2 rounded-t-lg px-4">
      <h1 className="text-2xl font-bold">Combat Tracker</h1>
      <div className="flex items-center space-x-4">
        Combat:
        <select onChange={(e) => dispatch(setCurrentCombat(e.target.value))} value={currentCombat?.id ?? "new"} className="px-4 py-2 rounded-md bg-neutral-700 hover:bg-neutral-600 focus:outline-none focus:ring-2 focus:ring-neutral-400">
          {
            combats.map((c, i) => {
              return <option key={i} value={c.id}>{c.name}</option>
            })
          }
        </select>
        <div className="gap-2 hidden lg:flex">
          <button className="px-4 py-2 rounded-md bg-slate-300 hover:bg-slate-500 focus:outline-none focus:ring-2 focus:ring-neutral-400 text-neutral-800"
            onClick={() => dispatch(addCombat(null))}
          ><SquarePlus /></button>
          <button className="px-4 py-2 rounded-md bg-slate-300 hover:bg-slate-500 focus:outline-none focus:ring-2 focus:ring-neutral-400 text-neutral-800"
            onClick={() => dispatch(addCombat(currentCombat))}
          >
            <CopyPlus />
          </button>
        </div>
      </div>
      <div className="flex items-center space-x-4">
        {
          edit ?
            <>
              <div className="flex items-center space-x-2">
                <button className="px-4 py-2 rounded-md bg-slate-300 hover:bg-slate-500 focus:outline-none focus:ring-2 focus:ring-neutral-400 text-neutral-800" onClick={() => dispatch(saveToDisk())}><FileDown /></button>
                <div >
                  <input type="file" style={{ display: 'none' }} ref={hiddenFile} onChange={(e) => handleUpload(e.target.files && e.target.files[0])} />
                  <button className="px-4 py-2 rounded-md bg-slate-300 hover:bg-slate-500 focus:outline-none focus:ring-2 focus:ring-neutral-400 text-neutral-800"
                    onClick={(e) => onUploadClick(e)}><FileUp /></button>
                </div>
              </div>
              <button className="px-4 py-2 rounded-md bg-emerald-600 hover:bg-emerald-500 focus:outline-none focus:ring-2 focus:ring-neutral-400" onClick={() => { dispatch(endEdit()); dispatch(save()) }}>
                <Save />
              </button>
            </>
            :
            <button className="px-4 py-2 rounded-md bg-red-600 hover:bg-red-500 focus:outline-none focus:ring-2 focus:ring-neutral-400" onClick={() => dispatch(startEdit())}>
              <Pencil />
            </button>
        }
      </div>
    </header>
  );
}
