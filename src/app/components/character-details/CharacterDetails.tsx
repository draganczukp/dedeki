import { Enemy } from "@/app/models/Model";
import { useAppSelector } from "@/app/state/hooks";
import { selectCurrentCombatant } from "@/app/state/stateSlice";
import classNames from "classnames";

export default function CharacterDetails() {
  const edit = useAppSelector((state) => state.globalState.edit);
  const character = useAppSelector(selectCurrentCombatant);
  if (edit) return '';
  return character ? (
    <section className="lg:w-1/2 w-full px-4 py-4 bg-neutral-800 rounded-r-lg" >
      <div className="h-full flex flex-col justify-center items-center gap-8" >
        <h2 className="text-3xl font-bold">Character Details</h2>
        <div className="w-full max-w-md bg-neutral-700 p-5 rounded-md shadow-lg">
          <div className="flex justify-between items-center mb-4">
            <h3 className="text-2xl font-bold">{character.name}</h3>
            <span className={classNames("font-bold", { "text-emerald-600": character.hp > 0, "text-red-600": character.hp <= 0 })}> {character.hp > 0 ? "Alive" : "Dead"}</span>
          </div>
          <div className="flex justify-between mb-4">
            <div>
              <p className="text-sm text-neutral-400">AC</p>
              <p className="text-lg font-bold">{character.ac}</p>
            </div>
            <div>
              <p className="text-sm text-neutral-400">HP</p>
              <p className="text-lg font-bold">{character.hp}/{character.hp_max}</p>
            </div>
            {'speed' in character && <div>
              <p className="text-sm text-neutral-400">Speed</p>
              <p className="text-lg font-bold">{character.speed}</p>
            </div>
            }
            {('special_speed' in character && (character.special_speed||0) > 0) && <div>
              <p className="text-sm text-neutral-400">Special Speed</p>
              <p className="text-lg font-bold">{character.special_speed} {character.special_speed_type}</p>
            </div>
            }
          </div>
          {'str' in character && <div className="flex justify-between mb-4">
            <div>
              <p className="text-sm text-neutral-400">STR</p>
              <p className="text-lg font-bold">{character.str}</p>
            </div>
            <div>
              <p className="text-sm text-neutral-400">DEX</p>
              <p className="text-lg font-bold">{character.dex}</p>
            </div>
            <div>
              <p className="text-sm text-neutral-400">CON</p>
              <p className="text-lg font-bold">{character.con}</p>
            </div>
            <div>
              <p className="text-sm text-neutral-400">INT</p>
              <p className="text-lg font-bold">{character.int}</p>
            </div>
            <div>
              <p className="text-sm text-neutral-400">WIS</p>
              <p className="text-lg font-bold">{character.wis}</p>
            </div>
            <div>
              <p className="text-sm text-neutral-400">CHA</p>
              <p className="text-lg font-bold">{character.cha}</p>
            </div>
          </div>
          }
          {'description' in character &&
            <div>
              <p className="text-sm text-neutral-400 mb-2">Description</p>
              <p className="text-neutral-300">
                {character.description}
              </p>
            </div>
          }
          {('attacks' in character && (character as Enemy).attacks.length > 0) &&
            <div className="mt-6">
              <h4 className="text-lg font-bold mb-2">Attacks</h4>
              {
                (character as Enemy).attacks.map((a, i) => <div key={i}>
                  <div className="space-y-2">
                    <p className="text-sm text-neutral-300 capitalize font-bold">{a.name}</p>
                    <div className="flex flex-wrap gap-y-2 items-center justify-between">
                      {a.bonus &&
                        <div className="flex w-1/2 items-center space-x-2">
                          <span className="text-sm text-neutral-400">To Hit:</span>
                          <span className="text-sm font-bold">{a.bonus || 0 >= 0 ? '+' : '-'}{a.bonus}</span>
                        </div>
                      }
                      {a.damage &&
                        <div className="flex w-1/2 items-center space-x-2">
                          <span className="text-sm text-neutral-400">Damage:</span>
                          <span className="text-sm font-bold">{a.damage}</span>
                        </div>
                      }
                      <div className="flex w-1/2 items-center space-x-2">
                        <span className="text-sm text-neutral-400">Range:</span>
                        <span className="text-sm font-bold">{a.range}</span>
                      </div>
                      <div className="flex w-1/2 items-center space-x-2">
                        <span className="text-sm text-neutral-400">Targets:</span>
                        <span className="text-sm font-bold">{a.targets}</span>
                      </div>
                      <div className="flex w-1/2 items-center space-x-2">
                        <span className="text-sm text-neutral-400">Save:</span>
                        <span className="text-sm font-bold">{a.save}</span>
                      </div>
                      <div className="flex w-1/2 items-center space-x-2">
                        <span className="text-sm text-neutral-400">Save Type:</span>
                        <span className="text-sm font-bold">{a.saveType}</span>
                      </div>
                      <p className="w-full bg-inherit focus:bg-neutral-600 hover:bg-neutral-600 focus:outline-none focus:ring-2 focus:ring-neutral-500 text-white">
                        {a.description}
                      </p>
                    </div>
                  </div>
                </div>
                )}
            </div>
          }
        </div>
      </div >
    </section >)
    : ''

}
