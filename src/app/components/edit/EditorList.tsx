import { Combat, Enemy, Initiative, PC } from "@/app/models/Model";
import { useAppDispatch, useAppSelector } from "@/app/state/hooks"
import { addCombatant, removeCombatant, selectCurrentEditedCombat, setCurrentCombatant, setEditedCombat } from "@/app/state/stateSlice";
import classNames from "classnames";
import { CircleX, Copy } from "lucide-react";
import { useState } from "react";

function EditorListEntry(
  { initiative, idx, updateCombat }:
    { initiative: Initiative, idx: number, updateCombat: (f: (c: Combat) => Combat) => void }) {
  const dispatch = useAppDispatch();

  function updateInit(initiatives: Initiative[], idx: number, f: (i: Initiative) => Initiative) {
    const newInit = [...initiatives];
    newInit[idx] = f(initiatives[idx]);
    return newInit;
  }

  const [init, setInit] = useState(initiative.roll);

  return <div className={classNames("flex items-center justify-between hover:bg-neutral-700 px-4 py-3 rounded-md cursor-pointer", {
    'bg-emerald-600/50': initiative.current && initiative.combatant.hp > 0,
    'bg-neutral-800': !initiative.current && initiative.combatant.hp > 0,
    'bg-red-600/25': initiative.combatant.hp <= 0,
    'bg-fuchsia-300': initiative.current && initiative.combatant.hp <= 0
  })}
    onClick={() => dispatch(setCurrentCombatant(initiative.combatant.id))}>
    <div className={classNames("flex items-center space-x-4", { "text-neutral-800": initiative.current && initiative.combatant.hp <= 0 })}>
      <div className="flex items-center space-x-2">
        <span className="text-sm">Initiative</span>
        <span className="text-lg font-bold">
          <input className="w-16 bg-inherit focus:bg-neutral-600 hover:bg-neutral-600 focus:outline-none focus:ring-2 focus:ring-neutral-500 text-white" type="number" value={init}
            onChange={(e) => setInit(parseInt(e.target.value))}
            onBlur={() => updateCombat((c) => ({ ...c, initiatives: updateInit(c.initiatives, idx, i => ({ ...i, roll: init })) }))}
          />
        </span>
      </div>
      <div className="flex items-center space-x-2">
        <span className="text-sm">AC</span>
        <span className="text-lg font-bold">{initiative.combatant.ac}</span>
      </div>
      <div className="flex items-center space-x-2">
        <span className="text-sm">HP</span>
        <span className="text-lg font-bold">{initiative.combatant.hp}/{initiative.combatant.hp_max}</span>
      </div>
      <span className="text-lg font-bold">{initiative.combatant.name}</span>
    </div>
    <div className="flex space-x-4">
      <div className="cursor-pointer p-4 bg-red-950 rounded-full" onClick={(_) => dispatch(removeCombatant(initiative.id))}><CircleX /></div>
      <div className="cursor-pointer p-4 bg-green-950 rounded-full" onClick={(_) => dispatch(addCombatant({ ...initiative.combatant }))}><Copy /></div>
    </div>
  </div>
}

export default function EditorList() {
  const dispatch = useAppDispatch();
  const edit = useAppSelector((state) => state.globalState.edit);
  const editedCombat = useAppSelector(selectCurrentEditedCombat);

  if (!edit) return '';

  function updateCombat(f: (combat: Combat) => Combat) {
    dispatch(setEditedCombat(f(editedCombat)));
  }

  return (
    <section className="w-3/4 px-6 py-4 bg-neutral-800 rounded-l-lg">
      <input className="text-xl font-bold w-full text-neutral-800 bg-transparent outline-none text-white" type="text" value={editedCombat?.name} onChange={e => updateCombat(c => ({ ...c, name: e.target.value }))} />
      <div className="w-full max-w-5xl mt-4 space-y-4">
        {editedCombat?.initiatives.toSorted((a, b) => b.roll - a.roll).map((init, i) => {
          return <EditorListEntry key={i} initiative={init} updateCombat={updateCombat} idx={i} />
        })}
      </div>
      <div className="flex justify-between gap-4">
        <button className="mt-4 w-full bg-emerald-600 hover:bg-emerald-700 focus:outline-none focus:ring-2 focus:ring-emerald-500 text-white font-bold py-2 px-4 rounded"
          onClick={() => dispatch(addCombatant({ name: "New PC", hp: 10, hp_max: 10, ac: 10 } as unknown as PC))}>Add PC</button>
        <button className="mt-4 w-full bg-emerald-600 hover:bg-emerald-700 focus:outline-none focus:ring-2 focus:ring-emerald-500 text-white font-bold py-2 px-4 rounded"
          onClick={() => dispatch(addCombatant({ name: "New Enemy", hp: 10, hp_max: 10, ac: 10, speed: 30, str: 10, dex: 10, con: 10, int: 10, wis: 10, cha: 10, attacks: [] } as unknown as Enemy))}>Add Enemy</button>
      </div>

    </section>
  );
}
