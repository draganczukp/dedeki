import { Attack, Enemy, PC } from "@/app/models/Model";
import { useAppDispatch, useAppSelector } from "@/app/state/hooks";
import { selectCurrentCombatant, selectCurrentEditedCombat, updateCombatant } from "@/app/state/stateSlice";
import { useState } from "react";

export default function CharacterDetails() {
  const dispatch = useAppDispatch();
  const edit = useAppSelector((state) => state.globalState.edit);
  const editedCombat = useAppSelector(selectCurrentEditedCombat);
  const character = useAppSelector(selectCurrentCombatant);
  const [editedCharacter, setEditedCharacter] = useState<Enemy>(character);
  if (!character) return '';
  if (!editedCharacter) setEditedCharacter(character);
  if(!editedCharacter) return '';
  if (!edit) return '';

  if (editedCharacter?.id !== character?.id) {
    const newCharacter = editedCombat.initiatives.find(i => i.combatant.id === character.id)
    if (newCharacter) setEditedCharacter(newCharacter.combatant as Enemy)
  }

  function updateCharacter(update: Partial<Enemy>) {
    if (!character) return;
    const newCharacter: Enemy = {
      ...character,
      ...update
    }
    setEditedCharacter(newCharacter)
  }

  function save() {
    if (!editedCharacter) return;
    dispatch(updateCombatant(editedCharacter))
  }

  function addAttack() {
    if (!editedCharacter || !('attacks' in editedCharacter)) return;
    const newCharacter: Enemy | PC = {
      ...editedCharacter,
      attacks: [...(editedCharacter as Enemy).attacks, { name: "ATTACK", activate: "action", description: "DESC", range: "RANGE", targets: "TARGETS" }]
    }
    setEditedCharacter(newCharacter)
    save()
  }


  return <section className="lg:w-1/2 w-full px-4 py-4 bg-neutral-800 rounded-r-lg" >
    <div className="h-full flex flex-col justify-center items-center gap-8" >
      <h2 className="text-3xl font-bold">Character Details</h2>
      <div className="w-full max-w-md bg-neutral-700 p-5 rounded-md shadow-lg">
        <div className="flex justify-between items-center mb-4">
          <h3 className="text-2xl font-bold">
            <input className="w-full bg-transparent outline-none text-white" placeholder="Name" type="text" value={editedCharacter?.name} onChange={(e) => updateCharacter({ name: e.target.value })} onBlur={(_) => save()} />
          </h3>
        </div>
        <div className="flex justify-between mb-4">
          <div>
            <p className="text-sm text-neutral-400">AC</p>
            <p className="text-lg font-bold">
              <input className="w-full bg-transparent outline-none text-white" min="1" max="30" placeholder="AC" type="number" value={editedCharacter?.ac} onChange={(e) => updateCharacter({ ac: parseInt(e.target.value || '0') })} onBlur={(_) => save()} />
            </p>
          </div>
          <div>
            <p className="text-sm text-neutral-400">HP</p>
            <p className="text-lg font-bold">
              <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="HP" type="number" value={editedCharacter?.hp} onChange={(e) => updateCharacter({ hp: parseInt(e.target.value || '0') })} onBlur={(_) => save()} />
              /
              <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="Max HP" type="number" value={editedCharacter?.hp_max} onChange={(e) => updateCharacter({ hp_max: parseInt(e.target.value || '0') })} onBlur={(_) => save()} />
            </p>
          </div>
          {'speed' in editedCharacter && <div>
            <p className="text-sm text-neutral-400">Speed</p>
            <p className="text-lg font-bold">
              <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="Speed" type="number" value={editedCharacter?.speed} onChange={(e) => updateCharacter({ speed: parseInt(e.target.value || '0') })} onBlur={(_) => save()} />
            </p>
          </div>
          }
        </div>
        {'str' in editedCharacter &&
          <>
            <div className="flex justify-between mb-4">
              <div>
                <p className="text-sm text-neutral-400">STR</p>
                <p className="text-lg font-bold">
                  <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="str" type="number" value={editedCharacter?.str} onChange={(e) => updateCharacter({ str: parseInt(e.target.value || '0') })} onBlur={(_) => save()} />
                </p>
              </div>
              <div>
                <p className="text-sm text-neutral-400">DEX</p>
                <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="dex" type="number" value={editedCharacter?.dex} onChange={(e) => updateCharacter({ dex: parseInt(e.target.value || '0') })} onBlur={(_) => save()} />
              </div>
              <div>
                <p className="text-sm text-neutral-400">CON</p>
                <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="con" type="number" value={editedCharacter?.con} onChange={(e) => updateCharacter({ con: parseInt(e.target.value || '0') })} onBlur={(_) => save()} />
              </div>
              <div>
                <p className="text-sm text-neutral-400">INT</p>
                <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="int" type="number" value={editedCharacter?.int} onChange={(e) => updateCharacter({ int: parseInt(e.target.value || '0') })} onBlur={(_) => save()} />
              </div>
              <div>
                <p className="text-sm text-neutral-400">WIS</p>
                <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="wis" type="number" value={editedCharacter?.wis} onChange={(e) => updateCharacter({ wis: parseInt(e.target.value || '0') })} onBlur={(_) => save()} />
              </div>
              <div>
                <p className="text-sm text-neutral-400">CHA</p>
                <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="cha" type="number" value={editedCharacter?.cha} onChange={(e) => updateCharacter({ cha: parseInt(e.target.value || '0') })} onBlur={(_) => save()} />
              </div>
            </div>
            <div>
              <textarea className="w-full min-h-4 bg-transparent outline-none text-white" placeholder="Extra details" value={editedCharacter?.description} onChange={(e) => updateCharacter({ description: e.target.value })} onBlur={(_) => save()} />
            </div>
            <div className="mt-6">
              <h4 className="text-lg font-bold mb-2">Attacks</h4>
              {
                (editedCharacter as Enemy).attacks.map((a, i) => <div key={i} className="mb-4">
                  <div className="space-y-2">
                    <div className="flex justify-between">
                      <p className="text-sm text-neutral-300 capitalize font-bold">
                        <input className="w-full bg-transparent outline-none text-white capitalize" min="0" placeholder="Name" type="text" value={a.name} onChange={(e) => {
                          const newAttacks = [...(character as Enemy).attacks]
                          newAttacks[i] = { ...a, name: e.target.value }
                          updateCharacter({ attacks: newAttacks })
                        }} onBlur={(_) => save()} />
                      </p>
                      <p className="text-sm text-neutral"><span className="font-bold">Activate: </span>
                        <select className="capitalize py-1 rounded-md bg-neutral-700 hover:bg-neutral-600 focus:outline-none focus:ring-2 focus:ring-neutral-400 inline-block" value={a.activate} onChange={(e) => {
                          const newAttacks = [...(character as Enemy).attacks]
                          newAttacks[i] = { ...a, activate: e.target.value as any }
                          updateCharacter({ attacks: newAttacks })
                        }}>
                          <option value="action">action</option>
                          <option value="bonus">bonus</option>
                          <option value="legendary_1">legendary 1</option>
                          <option value="legendary_2">legendary 2</option>
                          <option value="legendary_3">legendary 3</option>
                          <option value="mythic">mythic</option>
                        </select>
                      </p>
                    </div>
                    <div className="flex flex-wrap gap-y-2 items-center justify-between">
                      <div className="flex w-1/2 items-center space-x-2">
                        <span className="text-sm text-neutral-400">To Hit:</span>
                        <span className="text-sm font-bold">
                          <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="To Hit" type="number" value={a.bonus} onChange={(e) => {
                            const newAttacks = [...(character as Enemy).attacks]
                            newAttacks[i] = { ...a, bonus: parseInt(e.target.value || '0') }
                            updateCharacter({ attacks: newAttacks })
                          }}
                            onBlur={(_) => save()} />
                        </span>
                      </div>
                      <div className="flex w-1/2 items-center space-x-2">
                        <span className="text-sm text-neutral-400">Damage:</span>
                        <span className="text-sm font-bold">
                          <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="Damage" type="text" value={a.damage} onChange={(e) => {
                            const newAttacks = [...(character as Enemy).attacks]
                            newAttacks[i] = { ...a, damage: e.target.value }
                            updateCharacter({ attacks: newAttacks })
                          }}
                            onBlur={(_) => save()} />
                        </span>
                      </div>
                      <div className="flex w-1/2 items-center space-x-2">
                        <span className="text-sm text-neutral-400">Range:</span>
                        <span className="text-sm font-bold">
                          <input className="w-32 bg-transparent outline-none text-white" min="0" placeholder="Range" type="text" value={a.range} onChange={(e) => {
                            const newAttacks = [...(character as Enemy).attacks]
                            newAttacks[i] = { ...a, range: e.target.value }
                            updateCharacter({ attacks: newAttacks })
                          }}
                            onBlur={(_) => save()} />
                        </span>
                      </div>
                      <div className="flex w-1/2 items-center space-x-2">
                        <span className="text-sm text-neutral-400">Targets:</span>
                        <span className="text-sm font-bold">
                          <input className="w-32 bg-transparent outline-none text-white" min="0" placeholder="Targets" type="text" value={a.targets} onChange={(e) => {
                            const newAttacks = [...(character as Enemy).attacks]
                            newAttacks[i] = { ...a, targets: e.target.value }
                            updateCharacter({ attacks: newAttacks })
                          }}
                            onBlur={(_) => save()} />
                        </span>
                      </div>
                      <div className="flex w-1/2 items-center space-x-2">
                        <span className="text-sm text-neutral-400">Save:</span>
                        <span className="text-sm font-bold">
                          <input className="w-16 bg-transparent outline-none text-white" min="0" placeholder="Save" type="number" value={a.save} onChange={(e) => {
                            const newAttacks = [...(character as Enemy).attacks]
                            newAttacks[i] = { ...a, save: parseInt(e.target.value || '0') }
                            updateCharacter({ attacks: newAttacks })
                          }}
                            onBlur={(_) => save()} />
                        </span>
                      </div>
                      <div className="flex w-1/2 items-center space-x-2">
                        <span className="text-sm text-neutral-400">Save type:</span>
                        <span className="text-sm font-bold">
                          <select className="capitalize py-1 rounded-md bg-neutral-700 hover:bg-neutral-600 focus:outline-none focus:ring-2 focus:ring-neutral-400 inline-block" value={a.saveType} onChange={(e) => {
                            const newAttacks = [...(character as Enemy).attacks]
                            newAttacks[i] = { ...a, saveType: e.target.value as any }
                            updateCharacter({ attacks: newAttacks })
                          }}>
                            <option value="">none</option>
                            <option value="str">STR</option>
                            <option value="dex">DEX</option>
                            <option value="con">CON</option>
                            <option value="int">INT</option>
                            <option value="wis">WIS</option>
                            <option value="cha">CHA</option>
                          </select>
                        </span>
                      </div>
                    </div>
                    <div className="flex items-center space-x-2">
                      <textarea className="w-full bg-transparent outline-none text-white" placeholder="Description" value={a.description} onChange={(e) => {
                        const newAttacks = [...(character as Enemy).attacks]
                        newAttacks[i] = { ...a, description: e.target.value }
                        updateCharacter({ attacks: newAttacks })
                      }}
                        onBlur={(_) => save()} />
                    </div>
                  </div>
                </div>
                )}
              <button className="w-full mt-4 py-2 rounded-md bg-emerald-200 text-stone-900 hover:bg-neutral-500 focus:outline-none focus:ring-2 focus:ring-neutral-400" onClick={() => addAttack()}>Add attack</button>
            </div>
          </>
        }
      </div>
    </div >
  </section>

}

