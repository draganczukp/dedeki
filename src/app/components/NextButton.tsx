import { ChevronRight } from "lucide-react";
import { useAppDispatch } from "../state/hooks";
import { nextInitiative } from "../state/stateSlice";

export default function NextButton() {
  const dispatch = useAppDispatch();

  return (
    <button className="lg:w-24 lg:h-24 w-16 h-16 fixed right-8 bottom-8 rounded-full bg-emerald-600 flex justify-center items-center drop-shadow cursor-pointer" onClick={() => dispatch(nextInitiative())}>
      <ChevronRight className="text-xl" />
    </button>
  );
}
