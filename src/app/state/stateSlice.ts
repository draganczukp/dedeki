"use client";
import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { Combat, Enemy, PC } from '../models/Model'
import { generateId } from '../functions/generateId';

interface GlobalState {
	combats: Combat[];
	currentCombat?: Combat;
	edit: boolean;
	editedCombat?: Combat;
}

// NOTE: Initial state set only for dev purposes
const initialState: GlobalState = {
	edit: false,
	combats: [{
		id: generateId(),
		initiatives: [],
		currentRound: 1,
		name: ""
	}],
}

export const stateSlice = createSlice({
	initialState,
	name: 'globalState',
	reducers: {
		save: (state) => {
			window?.localStorage?.setItem('globalState', JSON.stringify(state));
		},
		load: (state) => {
			const savedState = window?.localStorage?.getItem('globalState') ? JSON.parse(localStorage.getItem('globalState')!) : null;

			if (!savedState) return;

			state.combats = savedState.combats;
			state.currentCombat = savedState.currentCombat;

		},
		addCombat: (state, action: PayloadAction<Combat | null>) => {
			const newCombat = action.payload ? {
				...action.payload,
				id: generateId()
			} : {
				id: generateId(),
				initiatives: [],
				currentRound: 1,
				name: "New Combat"
			};

			state.combats.push(newCombat);

			state.currentCombat = newCombat;
			state.edit = true;
		},
		setCurrentCombat: (state, action: PayloadAction<string | Combat>) => {
			if (typeof action.payload === 'string') {
				state.currentCombat = state.combats.find(c => c.id === action.payload);
			} else {
				state.currentCombat = action.payload;
			}
		},
		addCombatant: (state, action: PayloadAction<PC | Enemy>) => {
			const combat = selectCurrentEditedCombat({ globalState: state });
			if (!combat) return;

			const newCombatant = {
				...action.payload,
				id: generateId()
			};

			combat.initiatives.push({
				id: generateId(),
				combatant: newCombatant,
				roll: 0
			});
		},
		setRoll: (state, action: PayloadAction<{ id: string, roll: number }>) => {
			const combat = state.currentCombat;
			if (!combat) return;
			const combatant = combat.initiatives.find(c => c.id === action.payload.id);
			if (!combatant) return;
			combatant.roll = action.payload.roll;
		},
		nextInitiative: (state) => {
			const combat = selectCurrentCombat({ globalState: state });
			const initiatives = combat.initiatives.toSorted((a, b) => b.roll - a.roll);
			const currenctCombatantIndex = initiatives.findIndex(c => c.current);
			if (currenctCombatantIndex === -1 && initiatives.length > 0) {
				initiatives[0].current = true;
				return;
			}
			if (currenctCombatantIndex === -1) return;
			initiatives[currenctCombatantIndex].current = false;
			if (currenctCombatantIndex + 1 >= combat.initiatives.length) {
				combat.currentRound = (combat.currentRound || 1) + 1;
			}
			initiatives[(currenctCombatantIndex + 1) % combat.initiatives.length].current = true;
		},
		damage: (state, action: PayloadAction<{ id: string, damage: number }>) => {
			const combat = selectCurrentCombat({ globalState: state });
			if (!combat) return;
			const init = combat.initiatives.find(c => c.id === action.payload.id);
			if (!init) return;
			init.combatant.hp -= action.payload.damage;
			if (init.combatant.hp > init.combatant.hp_max) {
				init.combatant.hp = init.combatant.hp_max;
			}
		},
		startEdit: (state) => {
			state.edit = true;
		},
		endEdit: (state) => {
			state.edit = false;
			if (state.editedCombat) {
				const idx = state.combats.findIndex(c => c.id === state.editedCombat?.id);
				if (idx !== -1) {
					state.combats[idx] = state.editedCombat;
				}
				state.currentCombat = state.editedCombat;
				state.editedCombat = undefined;
			}
		},
		setEditedCombat: (state, action: PayloadAction<Combat>) => {
			state.editedCombat = action.payload;
		},
		setCurrentCombatant: (state, action: PayloadAction<string>) => {
			if (state.editedCombat) {
				state.currentCombat = state.editedCombat;
			}
			const combat = selectCurrentCombat({ globalState: state });
			if (!combat) return;
			const combatant = combat.initiatives.find(c => c.combatant.id === action.payload);
			if (!combatant) return;
			combat.initiatives.forEach(c => {
				c.current = false;
			})
			combatant.current = true;
		},
		updateCombatant: (state, action: PayloadAction<PC | Enemy>) => {
			let combat = state.editedCombat;
			if (!combat) combat = state.editedCombat = selectCurrentCombat({ globalState: state });
			const combatantIndex = combat.initiatives.findIndex(c => c.combatant.id === action.payload.id);
			if (combatantIndex === -1) return;
			if (action.payload.hp > action.payload.hp_max) action.payload.hp = action.payload.hp_max;
			combat.initiatives[combatantIndex].combatant = action.payload;
		},
		removeCombatant: (state, action: PayloadAction<string>) => {
			const combat = selectCurrentEditedCombat({ globalState: state });
			if (!combat) return;
			const combatantIndex = combat.initiatives.findIndex(c => c.id === action.payload);
			if (combatantIndex === -1) return;
			combat.initiatives.splice(combatantIndex, 1);
		},
		saveToDisk: (state) => {
			if (typeof window === 'undefined') return;
			const elt = document.createElement('a');
			elt.href = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(state));
			elt.download = 'combats.json';
			elt.click();
		},
		upload: (state, action: PayloadAction<string>) => {
			const savedState: Partial<GlobalState> = JSON.parse(action.payload as string);
			if (savedState.combats) {
				state.combats = savedState.combats;
				state.currentCombat = state.combats.find(c => c.id === state.currentCombat?.id);
				state.editedCombat = state.currentCombat;
			}
		}
	},
});

export const {
	load,
	save,
	addCombat,
	setCurrentCombat,
	addCombatant,
	setRoll,
	nextInitiative,
	damage,
	startEdit,
	setEditedCombat,
	endEdit,
	setCurrentCombatant,
	updateCombatant,
	removeCombatant,
	saveToDisk,
	upload
} = stateSlice.actions;

export const selectCombats = (state: { globalState: GlobalState }) => state.globalState.combats;

// NOTE: Get first combat for dev purposes
export const selectCurrentCombat = (state: { globalState: GlobalState }) => state.globalState.currentCombat || state.globalState.combats[0];

export const selectCombatant = (id: string) => (state: { globalState: GlobalState }) => selectCurrentCombat(state).initiatives.find(c => c.id === id);

export const selectCurrentCombatant = (state: { globalState: GlobalState }): Enemy => selectCurrentCombat(state).initiatives.find(c => c.current)?.combatant as Enemy || selectCurrentCombat(state).initiatives[0]?.combatant as Enemy;

export const selectCurrentEditedCombat = (state: { globalState: GlobalState }) => state.globalState.editedCombat || selectCurrentCombat(state);

export default stateSlice.reducer;
