FROM node:20-alpine AS builder

RUN apk add --no-cache libc6-compat
WORKDIR /app

COPY . .

ENV NEXT_TELEMETRY_DISABLED 1

RUN npm ci
RUN npm run build

FROM nginx as runner

COPY --from=builder /app/out/ /usr/share/nginx/html


