# Features
- Create and save multiple combat encounters
- Add your PCs and Enemies to the initiative
- Easily assign them the rolled initiative
- See the AC and HP of every combatant at a glance
- Deal damage (including negative damage for healing) to any combatant with ease
- See more detailed statblock for current combatant, including all their attributes, speed, and actions they can take
- Put some extra details in descriptions of everything to keep track of creature types, CR, and anything else you might need sometimes (but not always)
- Everything is stored locally in your browser, with easy export/import via a plain JSON file
- Extra features planned for future versions (see the README in the repo)

# Public instance
This app is hosted on GitLab Pages and available [here](https://draganczukp.gitlab.io/tracker)

# Installation instructions
## Build from source
- Clone this repo
- Make sure you have node and npm installed and at least node v20
- Run `npm install`
- Run `npm run build`
- The output - static files - are in `./out`, and can be ran on any web server (like nginx)

## Run via Docker
There is a pre-built image `registry.gitlab.com/draganczukp/tracker` with the usual `latest` tag, as well as `1-0` for the 1.0 release. The image uses no environment variables, and only needs the port `80` to serve the app.

You can run either via
```sh
docker run -p 80:80 registry.gitlab.com/draganczukp/tracker:latest
```

Or docker compose:
```yaml
version: '3'
services:
    tracker:
        image: registry.gitlab.com/draganczukp/tracker:latest
        container_name: tracker
        restart: unless-stopped
        ports:
            - "80:80"
```

# Future plans
- [ ] One-click add lair action
- [ ] Legendary actions display
- [ ] UI design improvements
- [ ] Migrate from redux to something less shit
- [ ] Get rid of FA and use only Lucide
- [ ] Code quality improvements
